// Ejercicios basicos (Del 1 al 5)
console.log("Ejercicio Basicos");
console.log("*-------------------------------------------------------------------------------------*");
let someObject = { str: "Starting javascript..." };
console.log(someObject);

let myName = "Malev";
console.log(myName);

let age = 22;
console.log(age);

let ignasiAge = 32;
let ageDiff = age - ignasiAge;
console.log(ageDiff);

if (age > 21) {
    console.log("You are older than 21");
} else {
    console.log("You are not older than 21");
}
if (ignasiAge <= age) {
    console.log("Ignasi is younger than you");
    if (ignasiAge = age)
        console.log("You have the same age as Ignasi");
} else {
    console.log("Ignasi is older than you");
}
/*------------------------------------------------------------------------*/
//Ejercicios con array
console.log("Ejercicio con array");
console.log("*----------------------------------------------------------------------------------*");

/*
1) Create an array with all the names of your class (including mentors).
Sort the array alphabetically.  (No entendi si habia que ordenarlo con codigo o crear uno ordenado)
Print the first element of the array in the console.
Print the last element of the array in the console.  
Print all the elements of the array in the console.
Use a "for" loop.
*/
console.log("Ejercicio 1");

let nombres;
nombres = ["Ana", "Alberto", "Bruno", "Felicia", "Giselle", "Mateo", "Pedro", "Zanda"];
console.log("El primero de la lista es " + nombres[0]);
console.log("El ultimo de la lista es " + nombres[nombres.length - 1]);
console.log("-------------Lista entera---------------");
for (var i = 0; i < nombres.length; i++) {
    console.log(nombres[i]);
}
console.log("---------------Fin Lista---------------");
/*
2)Create an array with all the ages of the students in your class.  
Iterate the array using a while loop, and then print every age in the console.  
Add a conditional inside the while loop to only print even numbers.  
Change the loop to use a "for" loop instead of a "while" loop.

*/
console.log("Ejercicio 2");
let edades;
var pares = [];
edades = [17, 20, 21, 24, 21, 19, 18, 21, 20, 19];
for (let i = 0; i < edades.length; i++) {
    if (edades[i] % 2 == 0) {
        pares.push(edades[i]);
    }
}
console.log("Las edades pares son: " + mostrar(pares));

/*---------------------------------------------------------------------*/
//Ejercicios con funciones
/*
3) Write a function which receives an array as a parameter and prints the lowest number in the array to the console.
*/
console.log("Ejercicio 3");
let num;
num = [321, 34, 4543, 6323234, 23534, 23423412, 1, 234, 567, 6, 4, 7, 3, 9, 323, 1, 0, -1, -341241, -99];

console.log("El minimo de la array num es: " + minimo(num));

function minimo(a) {
    let minimoActual;
    if (a.length == 0) {
        console.log("Es un Array vacio, RIP!")
        return 0;
    } else {
        minimoActual = num[0];
        for (let i = 0; i < a.length; i++) {
            if (a[i] < minimoActual) {
                minimoActual = a[i];
            }
        }
    }
    return minimoActual;
}

/*
4) Write a function which receives an array as a parameter and prints the biggest number in the array to the console.
*/
console.log("Ejercicio 4");
//copy pasteo la funcion anterior y cambio desigualdades, LITERAL
console.log("El maximo de la array num es: " + maximo(num));

function maximo(a) {
    let maximoActual;
    if (a.length == 0) {
        console.error("Es un Array vacio, RIP!");
    } else {
        maximoActual = num[0];
        for (let i = 0; i < a.length; i++) {
            if (a[i] > maximoActual) {
                maximoActual = a[i];
            }
        }
    }
    return maximoActual;
}
/*
5) Write a function which receives two parameters, an array and an index.  The function will print the value of the element at the given position (one-based) to the console.
*/
console.log("Ejercicio 5");
let invalido = 56;
let algunLado = 5;
console.log("El valor en el indice " + invalido + " es " + damevalor(num, invalido));
console.log("El valor en el indice " + algunLado + " es " + damevalor(num, algunLado));

function damevalor(array, indice) {
    if (indice >= array.length && indice < 0) {
        console.error("Invalido, campeon!");
    } else {
        return array[indice];
    }
}

/*
6) Write a function which receives an array and only prints the values that repeat.  
*/
console.log("Ejercicio 6");
var array = [3, 6, 67, 6, 23, 11, 100, 8, 93, 0, 17, 24, 7, 1, 33, 45, 28, 33, 23, 12, 99, 100];

function losRepes(cosa) { //complejidad de n*n 
    let repetidos = [];
    for (let i = 0; i < cosa.length; i++) { //complejidad n
        if (cantRepes(cosa, cosa[i]) > 1 && noEsta(repetidos, cosa[i])) { //complejidad 2n
            repetidos.push(cosa[i]);
        }
    }
    return repetidos;
}

console.log("Los repetidos en la Array son/es: " + mostrar(losRepes(array)));

function cantRepes(array, valor) {
    let cant = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] == valor) {
            cant++;
        }
    }
    return cant;
}

function noEsta(array, valor) {
    let res = true;
    for (let i = 0; i < array.length; i++) {
        if (array[i] == valor) {
            res = false;
        }
    }
    return res;
}




function mostrar(cosa) {
    var res = '[';
    for (let i = 0; i < cosa.length; i++) {
        let valor = cosa[i];
        if (i == cosa.length - 1) {
            res = res.concat(valor + ']');
            break;
        }
        res = res.concat(valor + ', ');
    }
    return res;
}


console.log("Ejercicio 7");
let myColor = ["Red", "Green", "White", "Black"];
//para este ej uso mostrar que implemente arriba
console.log("Los elementos en myColor son: " + mostrar(myColor));

/*---------------------------------------------------------------------*/
//Ejercicios con String

/*
1)  Write a JavaScript function that reverses a number. 
For example, if x = 32443 then the output should be 34423.
*/
//PRE que recibe un numero natural
function dameVuelta(nat) {
    let i = nat;
    let vuelta = [];
    while (i !== 0) {
        vuelta.push(i % 10);
        i = Math.floor(i / 10);
    }
    return vuelta;
}
let numerito = 323264123;
console.log("El numero " + numerito + " dado vuelta es: " + dameVuelta(numerito));


/*
2) Write a JavaScript function that returns a string in alphabetical order. 
For example, if x = 'webmaster' then the output should be 'abeemrstw'.  
Punctuation and numbers aren't passed in the string.
*/
//PRE no recibe puntuacion ni numero
let palabrita = "sinestesia";

function ordenadito(string) {
    while (ordenado(string)) {
        for (let i = 0; i < string.length - 1; i++) {
            if (string[i] > string[i + 1]) {
                //swapeo letras usando un aux para guardarlo
                // No puedo, no existe la operacion asignacion de char en un string!!! ... 
                let aux = string[i];
                string[i] = string[i + 1];
                string[i + 1] = aux;

            }
        }
    }
    return string;
}

function ordenado(palabra) {
    let res = true;
    for (let i = 0; i < palabra.length; i++) {
        if (palabra[i] > palabra[i + 1])
            res = false;
    }
    return res;
}

function volverArray(palabra) {
    let array = [];
    for (let i = 0; i < palabra.length; i++) {
        array.push(palabra[i]);
    }
    return array;
}

console.log("La parabra " + palabrita + " ordenado alfabeticamente es: " + (volverArray(palabrita).sort()).join(''));

/*
3) Write a JavaScript function that converts the first letter of every word to uppercase. 
For example, if x = "prince of persia" then the output should be "Prince Of Persia".
*/
//PRE recibe una cadena de texto no vacia
function ponerMayus(texto) {

    let desde = 0;
    let palabra = '';
    if (texto.length == 1) {
        return texto.toUpperCase();
    } else {
        for (let i = 1; i < texto.length; i++) {
            if (texto[i - 1] == ' ' || i == texto.length - 1) {
                let mayus = texto.substring(desde, desde + 1);

                let cuerpo;
                if (i == texto.length - 1) {
                    cuerpo = texto.substring(desde + 1, i + 1);
                } else {
                    cuerpo = texto.substring(desde + 1, i);
                } //Me di cuenta que el segundo parametro de substring es un menor estricto!
                palabra = palabra.concat(mayus.toUpperCase());

                palabra = palabra.concat(cuerpo);
                desde = i;
            }
        }
    }
    return palabra;
}
let texto = "hola,.............. cacahuate";
console.log("Le pongo mayus a |" + texto + "|: " + ponerMayus(texto));

/*
4)Write a JavaScript function that finds the longest word in a phrase. 
For example, if x = "Web Development Tutorial", then the output should be "Development".
*/
function palabraMasLarga(texto) {
    let desde = 0;
    let hasta = 0;
    let distanciaMasLarga = 0;
    let palabra;
    if (texto.length == 0) {
        let vacio = 'Error: No hay texto';
        return vacio;
    } else if (texto.length == 1) {
        return texto;
    } else {
        7
        for (let i = 1; i < texto.length; i++) {
            if (esPuntuacionOEspacio(texto[i - 1]) || i == texto.length - 1) {
                hasta = i - 1;
                if (hasta - desde > distanciaMasLarga) {
                    if (i == texto.length - 1) {
                        palabra = texto.substring(desde, texto.length);
                    } else {
                        palabra = texto.substring(desde, hasta);
                    }
                    distanciaMasLarga = hasta - desde;
                }

                desde = i;
            }
        }
    }
    return palabra;
}

function esPuntuacionOEspacio(char) {
    if (char == ' ' || char == '.' || char == ',' || char == '{' || char == '}' || char == '[' || char == ']' || char == '!' || char == '?' || char == '#' || char == '$' || char == '%') {
        return true;
    } else {
        return false;
    }
}

console.log("La palabra mas larga de |" + texto + "| es: " + palabraMasLarga(texto));