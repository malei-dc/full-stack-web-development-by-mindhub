// document.getElementById("table‐header").innerHTML = getHeadersHtml(data)
// JSON.stringify(data, null, 2);

function getRowHtml(miembros) {
    let todosLosRow = [];
    for (let i = 0; i < miembros.length; i++) {
        let nombre = agarrarNombreCompleto(miembros[i].first_name, miembros[i].middle_name, miembros[i].last_name);
        let porcentaje = agarrarPorcentaje(miembros[i].votes_with_party_pct)
        todosLosRow.push("<tr>" +  nombre + siNoEsNull(miembros[i].party)+ siNoEsNull(miembros[i].state) + siNoEsNull(miembros[i].seniority) + porcentaje + "</tr>");
    }
    return todosLosRow;
}

function siNoEsNull(valor){
    if (valor == null){
        return "<td>"+"</td>";
    }
    else{
        return "<td>" + valor + "</td>";
    }
}

function agarrarPorcentaje(numero) {
    if (numero == null)
        return "<td>"+"</td>";
    else
        return "<td>"+numero+"%"+"</td>";
}

function agarrarNombreCompleto(nombre, segundo, apellido){
    if(nombre != null && segundo != null && apellido !=null)
        return "<td>"+ apellido +", "+ nombre + " " + segundo +"</td>";
    else if (nombre != null && segundo != null && apellido ==null) 
        return "<td>"+ nombre + " " + segundo +"</td>";
    else if(segundo == null && nombre != null &&  apellido !=null)
        return "<td>"+ apellido +", "+ nombre +"</td>";
    else if(segundo != null && nombre == null &&  apellido !=null)
        return "<td>"+ apellido +", "+ segundo +"</td>";
    else if(segundo == null && nombre != null &&  apellido ==null)
        return "<td>"+ nombre +"</td>";
    else if(segundo == null && nombre == null &&  apellido !=null)
        return "<td>"+ apellido +"</td>";
    else if(segundo != null && nombre == null &&  apellido ==null)
        return "<td>"+ segundo +"</td>";
    else if (segundo == null && nombre == null &&  apellido ==null)
        return "<td>"+"</td>";
}

function renderRow(data){
    var html = getRowHtml(data.results[0].members);
    document.getElementById("table‐rows").innerHTML = html.join("");
}

renderRow(data)


   